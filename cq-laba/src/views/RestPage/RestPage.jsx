import React, { Fragment } from 'react'
import Button from "../../components/Button";
import {Icon, Table, Typography} from 'antd';
import Spin from "antd/es/spin";

const { Title } = Typography

class RestPage extends React.Component {
  state = {
    flPreloader: true
  };

  componentDidMount() {
    const {
      props: {
        getAlbums
      }
    } = this;

    setTimeout(() => getAlbums(), 2500);
  }

  addAlbum = () => {
    const {
      props: {
        createAlbum
      }
    } = this;

    const newAlbum = {
      title: 'The best title',
      userId: 1
    };

    createAlbum(newAlbum);
  }


  render() {

    const {
      props: {
        albums
      }
    } = this;

    const isLoading = false || albums.isFetching;

    const columns = [
      { title: 'id', dataIndex: 'id', key: 'id' },
      { title: 'Title', dataIndex: 'title', key: 'title' }
/*
      {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: item => <Button onClick={() => this.handleEdit(item)}>
          <Icon type='edit' />Edit
        </Button>
      }
*/
    ]

    return (
      <Fragment>
        <Title level={1}>
          Rest service
        </Title>

        {isLoading ? (<Spin />) : null}

        <Button onClick={() => this.addAlbum()}>
          Send Request
        </Button>

        {albums.list.length ? (
          <Table
            columns={columns}
            dataSource={albums.list}
            rowKey='id'
          />
          ) : 'No data'}

      </Fragment>
    )
  }

}

export default RestPage
