import { connect } from 'react-redux'
import AntdPage from './AntdPage'
import actions from '../../store/actions'

const mapStateToProps = state => ({
  ips: state.ips
})

const mapDispatchToProps = dispatch => ({
  addIp: data => dispatch(actions.ip.addIp(data)),
  updateIp: data => dispatch(actions.ip.updateIp(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AntdPage)
