import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col } from "antd";
import {Redirect} from 'react-router-dom';

// importing todo components
import AddTodoForm from "../../components/Todos/AddTodoForm";
import LoadTodoForm from "../../components/Todos/LoadTodoForm";
import ExitTodoForm from "../../components/Todos/ExitTodoForm";
import TodoList from "../../components/Todos/TodoList";

// importing our todo's action's
import { todoActions } from "../../store/actions/todo.js";

const TodosContainer = ({ todos, addTodo, loadTodo, removeTodo, toggleTodo }) => {
  // form submit handler to add a todo
  const handleformSubmit = todo => addTodo(todo);

  const handleLoadformSubmit = todo => loadTodo(todo);
// Todo removal handler
  const handleTodoRemoval = todo => removeTodo(todo);

  // Todo toggle handler
  const handleTodoToggle = todo => toggleTodo(todo);

    const handleExitformSubmit = todo => {


        sessionStorage.removeItem('token');
        return (<Redirect to={'/login'}/>)
    };


  return (
    <Row type="flex" justify="center" align="middle">
      <Col
        xs={{ span: 23 }}
        sm={{ span: 23 }}
        md={{ span: 21 }}
        lg={{ span: 20 }}
        xl={{ span: 18 }}
      >
        <AddTodoForm onFormSubmit={handleformSubmit} />

          <TodoList
            todos={todos}
            onTodoToggle={handleTodoToggle}
            onTodoRemoval={handleTodoRemoval}
          />

          <LoadTodoForm onFormSubmit={handleLoadformSubmit} />
          <ExitTodoForm onFormSubmit={handleExitformSubmit} />




      </Col>
    </Row>
  );
};

TodosContainer.propTypes = {
  todos: PropTypes.array.isRequired,
  addTodo: PropTypes.func.isRequired,
  removeTodo: PropTypes.func.isRequired,
  toggleTodo: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    todos: state.todo.todos
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addTodo: todoActions.addTodo,
      removeTodo: todoActions.removeTodo,
      toggleTodo: todoActions.toggleTodo,
      loadTodo: todoActions.loadTodo
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodosContainer);
