import React from "react";
//импорт списка из antD
import { List } from "antd";

import TodoItem from "../TodoItem";

const TodoList = ({ todos, onTodoRemoval, onTodoToggle }) => (
  <List
    dataSource={todos}
    renderItem={todo => (
      <TodoItem
        todo={todo}
        onTodoToggle={onTodoToggle}
        onTodoRemoval={onTodoRemoval}
      />
    )}
  />
);



export default TodoList;
