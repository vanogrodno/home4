import React from "react";
import { Form,  Row, Col, Button, Input } from "antd";



const AddTodoForm = ({ form, onFormSubmit }) => {
  const { getFieldDecorator } = form;

  // form submit handler
  const handleSubmit = e => {
    e.preventDefault();
    form.validateFields((err, todo) => {
      if (!err) {
        // сброс формы
        form.resetFields();

        // разварачиваем форму если все хорошо
        onFormSubmit(todo.name);
      }
    });
  };

  return (
    <Form
      onSubmit={e => handleSubmit(e)}

    >
      <Row >
          <Col>
              <Button type="primary" htmlType="submit" block>
                 Добавить
              </Button>
          </Col>
        <Col>
          <Form.Item>
            {getFieldDecorator("name", {
              rules: [
                {
                  required: true,
                  message: "Пусто!?"
                }
              ]
            })(
              <Input

              />
            )}
          </Form.Item>
        </Col>

      </Row>
    </Form>
  );
};

export default Form.create({ name: "AddTodoForm" })(AddTodoForm);
