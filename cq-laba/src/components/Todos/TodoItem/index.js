import React from "react";

import { Checkbox,  List, Button } from "antd";

const TodoItem = ({ todo, onTodoRemoval, onTodoToggle }) => {
  return (
    <List.Item
      actions={[
          <Button type="danger" onClick={() => onTodoRemoval(todo.id)}>
-
          </Button>
      ]}

    >
          <Checkbox
            defaultChecked={todo.completed}
            onChange={() => onTodoToggle(todo.id)}
          />

                  {todo.completed ? <del>{todo.name}</del> : todo.name}

    </List.Item>
  );
};



export default TodoItem;
